/**
 * 
 */
package com.springboot.angular.bikereservation.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.angular.bikereservation.model.Bike;
import com.springboot.angular.bikereservation.repository.BikeRepository;

/**
 * @author APTOL301466
 *
 */
@RestController
@RequestMapping("api/v1/bikes")
@CrossOrigin(origins = { "http://localhost:8080", "http://localhost:4200" })
public class BikeController {

	@Autowired
	private BikeRepository bikeRepository;

	@GetMapping
	public List<Bike> list() {
		return bikeRepository.findAll();
	}

	@PostMapping
	@ResponseStatus(HttpStatus.OK)
	public void create(@RequestBody Bike bike) {
		bikeRepository.save(bike);
	}

	@GetMapping("/{id}")
	public Bike get(@PathVariable("id") long id) {
		return bikeRepository.getOne(id);
	}

	@PutMapping("/{id}")
	public ResponseEntity<Bike> update(@PathVariable("id") long id, @RequestBody Bike bike) {

		Bike bikeItem = bikeRepository.getOne(id);
		final Bike updatedBikeRegistration = bikeRepository.save(bikeItem);
		return ResponseEntity.ok(updatedBikeRegistration);

	}

}
