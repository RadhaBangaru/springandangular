import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class BikeService {

  constructor(private http: HttpClient) { }
  //private baseUrl = '/api/v1/employees';
  getBikes() {

    return this.http.get('/server/api/v1/bikes');
  }

  getBike(id: number) {

    return this.http.get('/server/api/v1/bikes/' + id);
  }

  createBikeRegistration(bike) {
    let body = JSON.stringify(bike);
    return this.http.post('/server/api/v1/bikes', body, httpOptions);
  }
  updateBike(id: number, bike:any) {
    let body = JSON.stringify(bike);
    return this.http.put(`/server/api/v1/bikes/${id}`, body, httpOptions);
  }

}
