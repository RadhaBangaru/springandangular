import { Component, OnInit } from '@angular/core';
import { BikeService } from '../../services/bike.service';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-view-registration',
  templateUrl: './view-registration.component.html',
  styleUrls: ['./view-registration.component.css']
})
export class ViewRegistrationComponent implements OnInit {

  public bikeReg;
  bikeform: FormGroup;
  validMessage: string = "";
  constructor(private bikeService: BikeService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.getBikeReg(this.route.snapshot.params.id);
  }

  getBikeReg(id: number) {
    this.bikeService.getBike(id).subscribe(
      data => {
        this.bikeReg = data;
      },
      err => console.error(err),
      () => console.log('bikes loaded')
    );
  }
  updateBike(id: number, bike: any) {
    this.validMessage = "Your bike registration has been updated Successfuly. Thank you!";
    this.bikeService.updateBike(id, this.bikeform.value).subscribe(
      data => {
        this.bikeform.reset();
        return true;
      },
      err => console.error(err),
      () => console.log('bikes Updated')
    );
  }
}
